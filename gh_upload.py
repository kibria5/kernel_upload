import requests
import os
import glob

owner = "project-poison"
repo = "perf"
GH_TOKEN = os.environ.get("GH_TOKEN")
file_pattern = "perf*.zip"
file_path = glob.glob(file_pattern)[0]
tag = os.path.splitext(os.path.basename(file_path))[0]
release_title = "New release of Perf Kernel available"
url = f"https://api.github.com/repos/{owner}/{repo}/releases"
headers = {"Authorization": f"Token {GH_TOKEN}"}
data = {"tag_name": tag, "name": release_title, "body": "THIS WAS AN AUTOMATED RELEASE"}
response = requests.post(url, headers=headers, json=data)
release_id = response.json()["id"]
url = f"https://uploads.github.com/repos/{owner}/{repo}/releases/{release_id}/assets?name={os.path.basename(file_path)}"
headers = {"Authorization": f"Token {GH_TOKEN}", "Content-Type": "application/octet-stream"}
with open(file_path, "rb") as f:
    response = requests.post(url, headers=headers, data=f)
if response.ok:
    print("File uploaded to GitHub successfully.")
else:
    print(f"Failed to upload the file. Response: {response.text}")

